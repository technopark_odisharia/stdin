#!/usr/bin/perl

use v5.10;
use strict;
use warnings;
use Getopt::Long;
use Time::HiRes qw[ time sleep ];

$|++;

my $fh;
my $filename = "stdout";
my $timeout = 2;

sub append_stat {
	close($fh);
	open(my $fh_input, "<", $filename);
	my ($num_symbols, $num_rows) = (0, 0);
	while(<$fh_input>) {
		$num_rows++;
		$num_symbols += length($_) - 1;
	}
	my $buff = "$num_symbols $num_rows " . $num_symbols / $num_rows;
	print STDOUT $buff;
	close($fh_input);
}

$SIG{INT} = sub {
    state $last = 0;
    if ( ( time - $last ) > $timeout ) {
        $last = time;
        print STDERR "Double Ctrl+C for exit";
        return
    }
    append_stat();
    exit
};

$SIG{STOP} = sub {
    append_stat();
    die "\nInterupted by ^D\n";
};


GetOptions(
        "file=s"    => \$filename,
        "timeout:i" => \$timeout,
    );

print STDOUT "Get ready\n";
open($fh, ">", $filename);

while( <STDIN> ) {
	print $fh $_;
}

append_stat();
